const DiscordJS = require('discord.js')
const WOKCommands = require('wokcommands')
require('dotenv').config()

const client = new DiscordJS.Client()

client.on('ready', () => {

  console.log(client)

  console.log(`Connecting to Discord API... Please wait....`)

  const wok = new WOKCommands(client, {

    commandsDir: 'commands',
    featureDir: 'features',
    messagesPath: 'messages.json',
    testServers: [
      '801125364218200074',
      '775844088338972693',
    ],
    del: 20,
    defaultLangauge: 'english',
    ignoreBots: true,
    dbOptions: {
      keepAlive: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    },
    showWarns: false,
    disabledDefaultCommands: [
      // 'help',
      // 'command',
      // 'language',
      // 'prefix',
      // 'requiredrole'
    ]
  })
    .setBotOwner([
      '465228604721201158',
      '397666065066491905',
    ])
    .setDisplayName('UtilityBot')
    .setDefaultPrefix('!')
    .setColor('#58427C')
    .setMongoPath(process.env.MongoURI)
    .setCategorySettings([
      {
        name: 'Guild',
        emoji: '🏠'
      },
      {
        name: 'Music',
        emoji: '🎧'
      },
      {
        name: 'Fun',
        emoji: '🎮'
      },
      {
        name: 'Economy',
        emoji: '💸'
      },
      {
        name: 'Tools',
        emoji: '🔧'
      },
      {
        name: 'Development',
        emoji: '🚧'
      },
    ])

  wok.on('languageNotSupported', (message, lang) => {
    const { guild } = message
    console.log(`"${guild.name}" Attempted to set language to "${lang}"`)
  })

  wok.on('commandException', (command, message, error) => {
    const { guild } = message
    console.log(`An exception occured in "${guild.name}" when using command "${command.names[0]}"! The error is: ${error.message}`)
    console.error(error)
  })

  wok.on("databaseConnected", async (connection, state) => {
    const model = connection.models["wokcommands-languages", "wokcommands-prefixes"]

    const results = await model.countDocuments()
    console.log(results)
    console.log('Connected!')
  })

  client.user.setPresence({
    status: 'online',
    activity: {
      name: 'with WOKCommands',
      type: 'PLAYING'
    }
  })
})

client.login(process.env.ClientToken)