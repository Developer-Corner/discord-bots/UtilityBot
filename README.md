# UtilityBot
Discord bot built using `WOKCommands`

<img height="250" align="right" src="logo.png">

### Details

`UtilityBot` is built for us to try and learn how to use `WOKCommands`. As such, it will be built up over time as we learn how to implement more features.

The code displayed in this repo is shared freely for anyone who wants to host their own seperate instance.

### Self Hosting

You will need to make a file called `.env` in the base directory of the bot and place the follwing variables in it:

```env
MongoURI='mongodb+srv://<mongo_username>:<mongo_password>@<mongo_cluster>.ot1jr.mongodb.net/<database_name>?retryWrites=true&w=majority'
ClientToken='BOT_TOKEN'
```

## Commands

* Reaction Roles

| Command               | Description                                                                                                               | Usage                                                             |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------- |
| !rrmsg                 | Set the Reaction Roles Channel and Message         | !rrmsg #general 🎮 Gaming Channels                                          |
| !rr                 | Set the Reaction Roles         | !rr 🎮 @Gaming Gaming Channels                                           |

* Roles Commands

| Command               | Description                                                                                                               | Usage                                                             |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------- |
 | !addrole                 | Add a role to a user         | !addrole @johndoe Master Chief                                           |
 | !removerole                 | Remove a role from a user         | !removerole @johndoe Master Chief                                           |
 | !hasrole                 | Check if a user has a role         | !hasrole @johndoe Master Chief                                           |


 ### Links

 * [License](https://github.com/Discord-Coding-Community/UtilityBot/blob/main/LICENSE.md)
 * [Support](https://discord.me/discord-coding-community)
 * [Website](https://discord-coding-community.gitbook.io/discord-coding-community/)
