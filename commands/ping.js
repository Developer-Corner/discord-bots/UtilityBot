const DiscordJS = require('discord.js')

module.exports = {
    name: 'ping',
    aliases: ['echo', 'beep', 'status'],
    category: 'Tools',
    cooldown: '2s',
    requiredPermissions: ['SEND_MESSAGES'],
    callback: ({ message, args, text, client }) => {
        message.reply('Calculating ping...')
            .then(resultMessage => {
                const embed = new DiscordJS.MessageEmbed()
                    .setThumbnail('https://emoji.gg/assets/emoji/8299_Loading.gif')
                    .setTimestamp(new Date().toISOString())
                    .setColor('#58427C')

                let ping = resultMessage.createdTimestamp - message.createdTimestamp

                embed.addField('Bot Latency', `${ping}`)
                embed.addField('API Latency', `${client.ws.ping}`)

                resultMessage.edit({ embed })
                    .catch((error) => {
                        message.reply(`⚠️ Something went wrong. Please report this to my developers.\n\n$${error.message}`)
                    })
            })
    },
}