const DiscordJS = require('discord.js')

module.exports = {
    name: 'add',
    aliases: ['a'],
    description: 'Adds two numbers together',
    expectedArgs: '<Number 1> <Number 2>',
    minArgs: 2,
    maxArgs: 2,
    cooldown: '2s',
    category: 'Tools',
    requiredPermissions: ['SEND_MESSAGES'],
    callback: ({ channel, args }) => {

        const embed = new DiscordJS.MessageEmbed().setTitle('Math').setTimestamp()

        const number1 = parseInt(args[0])
        const number2 = parseInt(args[1])

        const sum = number1 + number2;

        embed.addField('Query', `${number1}+${number2}`)
        embed.addField('Sum', sum)

        channel.send({ embed })
    },
}