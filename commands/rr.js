const { fetchCache, addToCache } = require('../features/rr')
const messageSchema = require('../schemas/message')

module.exports = {
  name: 'reaction-role-settings',
  aliases: ['rrsettings', 'rrset', 'rr'],
  minArgs: 3,
  description: 'Set the reaction roles for the guild.',
  expectedArgs: '<Emoji> <Role name, tag, or ID> <Role display name>',
  cooldown: '2s',
  category: 'Guild',
  requiredPermission: ['MANAGE_ROLES', 'MANAGE_MESSAGES', 'MANAGE_GUILD'],
  callback: async ({ message, args }) => {
    const { guild } = message

    if (!guild.me.hasPermission('MANAGE_ROLES')) {
      message.reply('❌ I require the manage roles permission to be able to add or remove roles')
      .then((message) => {
        message.delete({
          timeout: 1000 * 10,
        })
      })

      return
    }

    let emoji = args.shift()
    let role = args.shift()
    const displayName = args.join(' ')

    if (role.startsWith('<@&')) {
      role = role.substring(3, role.length - 1)
      console.log(role)
    }

    const newRole =
      guild.roles.cache.find((r) => {
        return r.name === role || r.id === role
      }) || null

    if (!newRole) {
      message.reply('❌ Missing or Invalid role. See !help for command help.')
      .then((message) => {
        message.delete({
          timeout: 1000 * 10,
        })
      })

      return
    }

    role = newRole

    if (emoji.includes(':')) {
      const emojiName = emoji.split(':')[1]
      emoji = guild.emojis.cache.find((e) => {
        return e.name === emojiName
      })
    }

    const [fetchedMessage] = fetchCache(guild.id)
    if (!fetchedMessage) {
      message.reply('❌ Missing or invalid message. See !help for command help.')
      .then((message) => {
        message.delete({
          timeout: 1000 * 10,
        })
      })

      return
    }

    const newLine = `${emoji} ${displayName}`
    let { content } = fetchedMessage

    if (content.includes(emoji)) {
      const split = content.split('\n')

      for (let a = 0; a < split.length; ++a) {
        if (split[a].includes(emoji)) {
          split[a] = newLine
        }
      }

      content = split.join('\n')
    } else {
      content += `\n${newLine}`
      fetchedMessage.react(emoji)
    }

    fetchedMessage.edit(content)

    const obj = {
      guildId: guild.id,
      channelId: fetchedMessage.channel.id,
      messageId: fetchedMessage.id,
    }

    await messageSchema.findOneAndUpdate(
      obj,
      {
        ...obj,
        $addToSet: {
          roles: {
            emoji,
            roleId: role.id,
          },
        },
      },
      {
        upsert: true,
      }
    )

    addToCache(guild.id, fetchedMessage, emoji, role.id)

  },
}