module.exports = {
    name: 'hasrole',
    aliases: ['has', 'hasrank'],
    minArgs: 2,
    cooldown: '2s',
    description: 'Check if a user has a role.',
    category: 'Guild',
    expectedArgs: '<user mention> <role>',
    requiredPermissions: ['MANAGE_ROLES', 'MANAGE_MESSAGES'],
    callback: ({ message, args }) => {
        const { guild } = message
        const targetUser = message.mentions.users.first()

        if (!guild.me.hasPermission('MANAGE_ROLES')) {
            message.reply('❌ I require the manage roles permission to be able to add or remove roles')
            .then((message) => {
              message.delete({
                timeout: 1000 * 10,
              })
            })
            return
          }

        if (!targetUser) {
            message.reply('❌ Missing or Invalid user. See !help for command help.')
            .then((message) => {
                message.delete({
                  timeout: 1000 * 10,
                })
              })

            return
        }

        args.shift()

        const roleName = args.join(' ')

        const role = guild.roles.cache.find((r) => {
            return r.name === roleName
        })

        if (!role) {
            message.reply('❌ Missing or Invalid role. See !help for command help.')
            .then((message) => {
                message.delete({
                  timeout: 1000 * 10,
                })
              })
            return
        }

        const member = guild.members.cache.get(targetUser.id)

        if (member.roles.cache.get(role.id)) {
            message.reply(`${member} has the role: ${roleName}`)
        } else {
            message.reply(`${member} does not have the role: ${roleName}`)
                .catch((error) => {
                    message.reply(`⚠️ Something went wrong. Please report this to my developers.\n\n$${error.message}`)
                    .then((message) => {
                        message.delete({
                          timeout: 1000 * 10,
                        })
                      })
                })
        }
    }
}