const messageSchema = require('../schemas/message')
const { addToCache } = require('../features/rr')

module.exports = {
  name: 'reaction-role-message-settings',
  aliases: ['rrmsg', 'rrmessagesettings', 'rrmsgset'],
  minArgs: 1,
  description: 'Set the reaction roles message for the guild.',
  expectedArgs: '[Channel tag] <Message text>',
  cooldown: '2s',
  category: 'Guild',
  requiredPermissions: ['MANAGE_ROLES', 'MANAGE_MESSAGES', 'MANAGE_GUILD'],
  callback: async ({ message, args }) => {
    const { guild, mentions } = message
    const { channels } = mentions
    const targetChannel = channels.first() || message.channel

    if (channels.first()) {
      args.shift()
    }

    const text = args.join(' ')

    const newMessage = await targetChannel.send(text)

    if (guild.me.hasPermission('MANAGE_MESSAGES')) {
      message.delete()
    }

    if (!guild.me.hasPermission('MANAGE_ROLES')) {
      message.reply('❌ I require the manage roles permission to be able to add or remove roles')
      .then((message) => {
        message.delete({
          timeout: 1000 * 10,
        })
      })
      return
    }

    addToCache(guild.id, newMessage)

    new messageSchema({
      guildId: guild.id,
      channelId: targetChannel.id,
      messageId: newMessage.id,
    })
      .save()
      .catch((error) => {
        message
          .reply(`⚠️ Something went wrong. Please report this to my developers.\n\n$${error.message}`)
          .then((message) => {
            message.delete({
              timeout: 1000 * 10,
            })
          })
      })
  },
}