module.exports = {
    name: 'ban',
    aliases: ['b'],
    description: "Bans a member",
    expectedArgs: "<Target user's @> [Reason]",
    minArgs: 1,
    cooldown: '2s',
    category: 'Guild',
    permissions: ["BAN_MEMBERS"],
    guildOnly: true,
    callback: ({ message, args }) => {
        const { guild } = message

        if (!guild.me.hasPermission('BAN_MEMBERS')) {
            message.reply('❌ I require the ban members permission to be able to ban users')
                .then((message) => {
                    message.delete({
                        timeout: 1000 * 10,
                    })
                })

            return
        }

        const targetUser = message.mentions.members.first();

        if (!targetUser) {
            message.reply(':x: Missing or Invalid user. See !help for command help.')
                .then((message) => {
                    message.delete({
                        timeout: 1000 * 10,
                    })
                })

            return
        }

        if (!targetUser.bannable) {
            message.reply("❌ I do not have permission to ban that user!")
                .then((message) => {
                    message.delete({
                        timeout: 1000 * 10,
                    })
                })

            return
        }

        args.shift();
        const reason = args.join(" ")

        targetUser.ban({
            reason,
            days: 5,
        });

        message.reply(`☑️ Successfully Banned ${targetUser}!`)
    },
}